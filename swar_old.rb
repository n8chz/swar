require "matrix"
require "nokogiri"

class Swar
  class VertexNotFound < Exception
  end
  
  class NoOperator < Exception
  end
  
  class MultipleOperators < Exception
  end
  
  class MisplacedOperator < Exception
  end
  
  OPERATORS = {"sa" => 2, "wa" => 1, "nullo" => 0, "wr" => -1, "sr" => -2}

  attr_accessor :graph

  def initialize(vertices = [])
    @graph = {}
    @edge_count = 0
    @total_flow = 0
    @x, @y, @w, @h = [-1, -1, 2, 2]
    vertices.each do |vertex|
      add_vertex! vertex
    end
  end

  # NOTE: methods on vertices take vertex tags as parameters:
  # see https://en.wikipedia.org/wiki/Graph_(abstract_data_type)#Operations

  def adjacent?(x, y)
    f, s = [x.to_sym, y.to_sym].sort
    raise VertexNotFound unless vertices.include? f
    raise VertexNotFound unless vertices.include? s
    !!@graph[f][:edges][s]
  end

  def neighbors(x)
    xs = x.to_sym
    raise VertexNotFound unless has_vertex? xs
    before = @graph.select {|k, v| v[:edges].keys.include?(xs)}.keys
    after = @graph[xs][:edges].keys
    before+after-[@far_point]
  end

  def add_vertex!(x)
    xs = x.to_s.to_sym
    @graph[xs] = new_vertex
    add_edge!(xs, @far_point, -2) if @far_point
    self
  end

  def remove_vertex!(x)
    xs = x.to_sym
    @graph.delete xs
    @graph.each do |name, _|
      @graph[name][:edges].delete xs
    end
    self
  end

  def add_edge!(x, y, flow = 1)
    # puts("empty string passed") if x.to_s == ""
    # puts("empty string passed") if y.to_s == ""
    add_vertex!(x) unless has_vertex?(x)
    add_vertex!(y) unless has_vertex?(y)
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    @graph[f][:edges][s] = flow
    @total_flow = flow
    @edge_count += 1
    self
  end

  def remove_edge!(x, y)
    raise VertexNotFound unless has_vertex?(x) && has_vertex?(y)
    f, s = [x.to_sym, y.to_sym].sort
    @graph[f][:edges].delete s
    self
  end

  def get_vertex_value(x)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value]
  end

  def set_vertex_value!(x, val)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value] = val
    self
  end

  # end of methods in Wikipedia's list

  # additional "topological" methods:

  def vertices
    @graph.keys
  end

  # A "stretcher" is defined as a triad of vertices in which
  # two of the three repel each other, while the third is attracted
  # to the first two.
  # Return a list of all such triads in the Swar
  def stretchers
    vertices.combination(3).map do |c|
      w =  c.combination(2).map {|x, y| flow(x, y) <=> 0}
      c.zip(w).sort_by {|_, s| s}
    end.map(&:transpose).select {|_, s| s == [-1, 1, 1]}.map(&:first)
  end
  


  # flow assigned to edge between two vertices
  def flow(x, y)
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    raise VertexNotFound unless has_vertex?(f) && has_vertex?(s)
    return 0 unless @graph[f]
    @graph[f][:edges][s] || 0
  end

  # quality is correlation between distance and attraction
  # lower is better, so we return the additive inverse of r
  def quality
    n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
    min_dist = 2.0
    @graph.to_a.combination(2).map do |v1, v2|
      n1, p1 = v1
      n2, p2 = v2
      dist = (p1[:location]-p2[:location]).norm
      min_dist = [dist, min_dist].min
      flow = flow(n1, n2)
      [dist, flow]
    end.each do |x, y|
      n += 1
      sx += x
      sy += y
      sxy += x*y
      sx2 += x*x
      sy2 += y*y
      # puts "#{((n*sx2-sx*sx)*(n*sy2-sy*sy))}"
    end
    r = (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
    [min_dist, -r]
  end

  # qap_cost is defined as
  # the sum of the distances multiplied by the corresponding flows (flows).
  # Goal(s) include(s) minimizing this metric.
  # see https://en.wikipedia.org/wiki/Quadratic_assignment_problem
  def qap_cost
    vertices.combination(2).map do |x, y|
      dist = (@graph[x][:location]-@graph[y][:location]).norm
      flow(x, y)*dist
    end.sum
  end

  def monte_carlo!(timeout = 10.0)
    md, q = quality
    t0 = Time.new.to_f
    n = 0
    while Time.new.to_f-t0 < timeout
      backup = @graph.clone
      @graph.each do |k, v|
        v[:location] = random_unit_circle_vector
      end
      md2, q2 = quality
      n += 1
      if q2 > q # && md2 > md
        puts "#{md}, #{q} -> #{md}, #{q2} (after #{n} trials)"
        q = q2
        md = md2
      else
        @graph = backup
      end
    end
  end

  def to_s
    @graph.to_s
  end

  # read single line of SWAR file into self
  def process_swar_line!(line)
    tokens = line.sub(/#.*/,"").strip.downcase.split(/\s/)
    return self if tokens.empty? # whitespace lines ok but inconsequential
    parts = tokens.chunk do |x|
      OPERATORS.keys.include? x
    end.to_a.map(&:pop)
    raise NoOperator if parts.count == 1
    raise MisplacedOperator if parts.count == 2
    raise MultipleOperators if parts.count > 3
    left, op, right = parts
    left.product(right).each do |x, y|
      # puts "about to add #{x}, #{y}"
      add_edge!(x.to_sym, y.to_sym, OPERATORS[op[0]])
    end
    self
  end

  def self.from_file(filename)
    stream = File.new(filename, "r")
    swar = Swar.new
    while (line = stream.gets)
      swar.process_swar_line!(line)
    end
    swar
  end

  def self.generate_random(vertex_count, density)
    raise ArgumentError unless vertex_count.integer? && vertex_count.positive?
    raise ArgumentError unless (0.0..1.0).include? density
    edge_count = (density*vertex_count*(vertex_count-1)/2.0).round
    tags = (1..vertex_count).map(&:to_s).map(&:to_sym)
    swar = Swar.new(tags)
    tags.combination(2).to_a.sample(edge_count).each do |x, y|
      # puts "about to add #{x}, #{y}"
      swar.add_edge!(x, y, [-2, -1, 1, 1].sample)
    end
    swar
  end

  def generate_svg(file_name)
    size = Math.sqrt(@h*@w)
    text_size = size/20
    stroke_width = size/200
    @x -= size*0.1
    @y -= size*0.05
    @w *= 1.2
    @h *= 1.1
    file_name += "%+010.4f" % @self_flow
    file_name += ".svg" unless file_name.end_with? "svg"
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.svg("viewBox" => "#{@x} #{@y} #{@w} #{@h}", "xmlns" => "http://www.w3.org/2000/svg") {
        @graph.each do |k1, v1|
          x1, y1 = v1[:location].to_a
          xml.text_("x" => x1, "y" => y1, "style" => "font-size: #{text_size}px;", "text-anchor" => "middle") {xml.text k1}
          v1[:edges].each do |k2, v2|
            next if flow(k1, k2).zero?
            x2, y2 = @graph[k2][:location].to_a
            xml.line("stroke" => "black", "stroke-width" => "#{stroke_width}", "x1" => x1, "y1" => y1, "x2" => x2, "y2" => y2)
          end
        end
      }
    end
    File.open(file_name, "w") do |file|
      file.write(builder.to_xml)
    end
  end

  def has_vertex?(x)
    vertices.include?(x.to_s.to_sym)
  end

  # see https://en.wikipedia.org/wiki/Principal_component_analysis#Dimensionality_reduction
  def pca!(self_flow = 3)
    @self_flow = self_flow
    n = @graph.count
    data = Matrix.build(n, n) do |i, j|
      i == j ? @self_flow : flow(vertices[i], vertices[j])
    end
    xtx = data.transpose*data
    w = Matrix.columns(Matrix::EigenvalueDecomposition.new(xtx).eigenvectors)
    coords = data*Matrix.columns(w.column_vectors.take(2))
    # puts "coords.transpose: #{coords.transpose}"
    # exit
    xs, ys = coords.to_a.transpose.map(&:minmax)
    @x, @w = xs
    @y, @h = ys
    @w -= @x
    @h -= @y
    @graph.zip(coords.row_vectors) do |v, c|
      v[1][:location] = Vector.elements c
    end
  end

  private

  def new_vertex
    {location: random_unit_circle_vector, edges: {}}
  end

  def random_unit_circle_vector
    loop do
      loc = Vector[rand(-1.0..1.0), rand(-1.0..1.0)]
      return loc if loc.norm <= 1.0
    end
  end
end

