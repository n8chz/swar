require "matrix"
require "array-symmetries"
# require "nokogiri"

class Swar
  class VertexNotFound < Exception
  end
  
  class NoOperator < Exception
  end
  
  class MultipleOperators < Exception
  end
  
  class MisplacedOperator < Exception
  end
  
  OPERATORS = {"sa" => 2, "wa" => 1, "nullo" => 0, "wr" => -1, "sr" => -2}

  attr_accessor :graph

  def initialize(vertices = [])
    @graph = {}
    @edge_count = 0
    @total_flow = 0
    # @x, @y, @w, @h = [-1, -1, 2, 2]
    vertices.each do |vertex|
      add_vertex! vertex
    end
    setup_grid!
  end

  # NOTE: methods on vertices take vertex tags as parameters:
  # see https://en.wikipedia.org/wiki/Graph_(abstract_data_type)#Operations

  def adjacent?(x, y)
    f, s = [x.to_sym, y.to_sym].sort
    raise VertexNotFound unless vertices.include? f
    raise VertexNotFound unless vertices.include? s
    !!@graph[f][:edges][s]
  end

  def neighbors(x)
    xs = x.to_sym
    raise VertexNotFound unless has_vertex? xs
    before = @graph.select {|k, v| v[:edges].keys.include?(xs)}.keys
    after = @graph[xs][:edges].keys
    before+after
  end

  def add_vertex!(x)
    xs = x.to_s.to_sym
    @graph[xs] = new_vertex
    self
  end

  def remove_vertex!(x)
    xs = x.to_sym
    @graph.delete xs
    @graph.each do |name, _|
      @graph[name][:edges].delete xs
    end
    self
  end

  def add_edge!(x, y, flow = 1)
    # puts("empty string passed") if x.to_s == ""
    # puts("empty string passed") if y.to_s == ""
    add_vertex!(x) unless has_vertex?(x)
    add_vertex!(y) unless has_vertex?(y)
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    @graph[f][:edges][s] = flow
    @total_flow += flow
    @edge_count += 1
    self
  end

  def remove_edge!(x, y)
    raise VertexNotFound unless has_vertex?(x) && has_vertex?(y)
    f, s = [x.to_sym, y.to_sym].sort
    @graph[f][:edges].delete s
    self
  end

  def get_vertex_value(x)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value]
  end

  def set_vertex_value!(x, val)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value] = val
    self
  end

  # end of methods in Wikipedia's list

  # Using symmetry groups:

  # The eight symmetries of the layout square
  def layout_symmetries
    Array.patterns(@n).map do |sym|
      @layout.map do |v, coords|
        i, j = coords
        [v, sym[i][j]]
      end.to_h
    end
  end

  def normalized_improve!
    old_layout = @layout.clone
    q = qap_cost
    loop do
      setup_grid!
      break if qap_cost < q
    end
    # pick symmetric layout most similar to old layout:
    @layout = layout_symmetries.min_by do |sym|
      old_layout.map do |v, coords|
        (Vector.elements(coords)-Vector.elements(sym[v])).norm
      end.sum
    end
  end

  def display
    m = @layout.keys.map(&:length).max
    a = Array.new(@n) {Array.new(@n, "".ljust(m))}
    @layout.each do |vertex, coords|
      # puts "coords: #{coords}"
      r, c = coords
      a[r][c] = vertex.to_s.ljust(m)
    end
    puts "#{a.map {|r| r.join(" ")}.join("\n")}\n"
  end

  def ratchet(timeout = 60)
    start = Time.new.to_f
    loop do
      backup = @layout.clone
      normalized_improve!
      display
      puts " q=#{qap_cost}\n\n"
      t = Time.new.to_f-start
      if t > timeout
        @layout = backup
        break
      end
    end
    self
  end

  # additional "topological" methods:

  def vertices
    @graph.keys
  end

  # see https://en.wikipedia.org/wiki/Centrality#Harmonic_centrality
  def harmonic_centralities
    @graph.each_with_object({}) do |kv, edges|
      v1, props = kv
      props[:edges].each do |v2, flow|
        edges[v1] ||= 0
        edges[v2] ||= 0
        edges [v1] += flow
        edges [v2] += flow
      end
    end
  end

  # A "stretcher" is defined as a triad of vertices in which
  # two of the three repel each other, while the third is attracted
  # to both of the first two.
  # Return a list of all such triads in the Swar
  def stretchers
    vertices.combination(3).map do |c|
      w =  c.combination(2).map {|x, y| flow(x, y) <=> 0}
      c.zip(w).sort_by {|_, s| s}
    end.map(&:transpose).select {|_, s| s == [-1, 1, 1]}.map(&:first)
  end
  


  # flow assigned to edge between two vertices
  def flow(x, y)
    return 0 if x == y
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    raise VertexNotFound unless has_vertex?(f) && has_vertex?(s)
    return 0 unless @graph[f]
    @graph[f][:edges][s] || 0
  end

  # qap_cost is defined as
  # the sum of the distances multiplied by the corresponding flows (flows).
  # Goal(s) include(s) minimizing this metric.
  # see https://en.wikipedia.org/wiki/Quadratic_assignment_problem
  def qap_cost(layout = @layout)
    @graph.map do |v1, p1|
      p1[:edges].map do |v2, p2|
        layout[v1].dist_to(layout[v2])*flow(v1, v2)
      end.sum
    end.sum
  end

  def to_s
    @graph.to_s
  end

  # read single line of SWAR file into self
  def process_swar_line!(line)
    tokens = line.sub(/#.*/,"").strip.downcase.split(/\s/)
    return self if tokens.empty? # whitespace lines ok but inconsequential
    parts = tokens.chunk do |x|
      OPERATORS.keys.include? x
    end.to_a.map(&:pop)
    raise NoOperator if parts.count == 1
    raise MisplacedOperator if parts.count == 2
    raise MultipleOperators if parts.count > 3
    left, op, right = parts
    left.product(right).each do |x, y|
      # puts "about to add #{x}, #{y}"
      add_edge!(x.to_sym, y.to_sym, OPERATORS[op[0]])
    end
    self
  end

  # a couple instantiators:

  def self.from_file(filename)
    stream = File.new(filename, "r")
    swar = new
    while (line = stream.gets)
      swar.process_swar_line!(line)
    end
    swar.setup_grid!
  end

  def self.generate_random(vertex_count, density)
    raise ArgumentError unless vertex_count.integer? && vertex_count.positive?
    raise ArgumentError unless (0.0..1.0).include? density
    edge_count = (density*vertex_count*(vertex_count-1)/2.0).round
    tags = (1..vertex_count).map(&:to_s).map(&:to_sym)
    swar = Swar.new(tags)
    tags.combination(2).to_a.sample(edge_count).each do |x, y|
      # puts "about to add #{x}, #{y}"
      swar.add_edge!(x, y, [-2, -1, 1, 1].sample)
    end
    swar.setup_grid!
  end

  def has_vertex?(x)
    vertices.include?(x.to_s.to_sym)
  end

  def process!(timeout = 60, m = :swap2!)
    q = qap_cost
    start = Time.new.to_f
    loop do
      backup = @layout.clone
      improve!(timeout, m)
      q2 = qap_cost
      t = Time.new.to_f-start
      if t > timeout
        @layout = backup
        break
      end
      puts "\"#{m}\",#{t},#{q},#{q2}"
      q = q2
    end
    self
  end

  def improve!(timeout = 60, m = :swap2!)
    q = qap_cost
    start = Time.new.to_f
    loop do
      backup = @layout.clone
      send(m)
      break if qap_cost < q
      if Time.new.to_f-start > timeout
        @layout = backup
        break
      end
    end
    self
  end

  # vertex properties:

  def flow_degree(vertex)
    # For our purposes, degree will be sum of flows
    @graph.map do |k, v|
      if k == vertex
        v[:edges].values.sum
      else
        v[:edges][vertex] || 0
      end
    end.sum
  end

  def distance_from_center(vertex)
    (Vector.elements(@layout[vertex])-@center).norm
  end

  # report generators

  def show_vertex_table
    size = @graph.keys.map(&:length).max
    puts "\nvertex".ljust(size)+"            flow_degree  distance_from_center"
    @graph.keys.each do |v|
      puts v.to_s.ljust(size)+"%22.9f"%flow_degree(v)+"%22.9f"%distance_from_center(v)
    end
  end

  # branch-and-bound utilities:

  def branch_and_bound!
    @layout = {}
    while @layout.count < vertices.count do
      open_vertices = vertices-@layout.keys
      open_cells = @grid-@layout.values
      first_assignments = open_vertices.product(open_cells)
      assignment = first_assignments.min_by do |v1, loc|
        # puts "v1: #{v1}, loc: #{loc}"
        vertices.map do |v2|
          flow = flow(v1, v2)<=>0
          if flow.zero?
            dist = 0
          elsif @layout.keys.include? v2
            dist = @layout[v2].dist_to(loc)
          else
            dists = open_cells.map {|corner| loc.dist_to(corner)}
            dist = flow.negative? ? dists.min : dists.max
          end
          flow*dist
        end.sum
      end
      @layout.merge!([assignment].to_h)
      puts "placed #{assignment[0]}"
    end
    self
  end


  # assignment generators that can be passed to #process:

  def setup_grid!
    n = vertices.count
    @n = Math.sqrt(n+1).ceil
    @center = Vector[@n/2.0, @n/2.0]
    @grid = (0...@n).to_a.product((0...@n).to_a)
    @layout = vertices.zip(@grid.shuffle).to_h
    self
  end

  def swap2!
    a, b = @grid.sample(2)
    x = @layout.key(a)
    y = @layout.key(b)
    @layout[x] = b if x
    @layout[y] = a if y
  end

  private

  def new_vertex
    {edges: {}}
  end

  def place_vertex!(vertex, square)
    @layout.index(square)
  end
end

class Array
  def dist_to(other)
    ds = zip(other).map do |x, y|
      (x-y)*(x-y)
    end.sum
    Math.sqrt(ds)
  end
end

