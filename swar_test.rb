require 'minitest/autorun'

require_relative 'swar'

class SwarTest < Minitest::Test
  def test_adjacent
    skip
    adjacent = Swar.new(["x", "y"]).add_edge!(:x, :y, 2).adjacent?(:x, :y)
    assert adjacent
  end

  def test_adjacent_for_order_independence
    skip
    adjacent = Swar.new(["x", "y"]).add_edge!(:x, :y, 2).adjacent?(:y, :x)
    assert adjacent
  end

  def test_add_edge_for_order_independence
    skip
    adjacent = Swar.new(["x", "y"]).add_edge!(:y, :x, 2).adjacent?(:x, :y)
    assert adjacent
  end

  def test_neighbors_when_there_are_no_neighbors
    skip
    assert_equal [], Swar.new(["y", "x"]).neighbors(:x)
  end

  def test_add_vertex
    skip
    expected = [:goo]
    value = Swar.new.add_vertex!(:goo).add_vertex!(:foo).add_edge!(:foo, :goo, 2).neighbors(:foo)
    assert_equal expected, value
  end

  def test_remove_vertex
    skip
    assert_raises(Swar::VertexNotFound) {Swar.new([:foo, :goo]).remove_vertex!(:foo).get_vertex_value(:foo)}
    refute Swar.new([:foo, :goo]).remove_vertex!(:goo).get_vertex_value(:foo)
  end

  def test_add_edge
    skip
    value = Swar.new([:foo, :bar, :baz]).add_edge!(:bar, :foo, -1).add_edge!(:baz, :bar).neighbors(:bar)
    expected = [:baz, :foo]
    assert_equal expected, value.sort
  end

  def test_remove_edge
    skip
    value = Swar.new([:foo, :bar, :baz]).add_edge!(:bar, :foo, -1).add_edge!(:baz, :bar).remove_edge!(:foo, :bar).neighbors(:bar)
    expected = [:baz]
    assert_equal expected, value
  end

  def test_vertex_value
    skip
    swar = Swar.new([:foo, :bar, :baz]).add_edge!(:bar, :foo, -1).add_edge!(:baz, :bar)
    value = swar.set_vertex_value!(:bar, {a: 1, b: 2}).get_vertex_value(:bar)
    expected = {a: 1, b: 2}
    assert_equal expected, value
  end

  def test_swar_line
    skip
    swar = Swar.new.process_swar_line!("a b sa c d e")
    swar.process_swar_line!("foo goo wr bar baz")
    assert_equal swar.neighbors(:baz).sort, [:foo, :goo]
    assert_equal swar.neighbors(:a).sort, [:c, :d, :e]
  end

  def test_swar_from_file
    skip
    swar = Swar.from_file("sample.swar")
    value = swar.neighbors(:a).sort
    expected = [:b, :c, :f, :g, :i]
    assert_equal value, expected
    assert_equal(-2, swar.flow(:a, :c))
    assert_equal 2, swar.flow(:i, :b)
  end

  def test_stretcher
    skip
    (10).times do
      Swar.generate_random(rand(10..100), rand(0.1..0.6)).stretchers
    end
  end

  def test_qap
    skip
    (10).times do
      actual = Swar.generate_random(rand(10..100), rand(0.1..0.6)).qap_cost
      assert_kind_of(Numeric, actual)
    end
  end

  def test_process
    skip
    swar1 = Swar.generate_random(rand(10..100), rand(0.1..0.6))
    # swar2 = Marshal.load(Marshal.dump(swar1)) # h/t https://stackoverflow.com/a/8206537/948073
    swar3 = Marshal.load(Marshal.dump(swar1))
=begin
    puts
    swar1.process!(20, :swap2!)
    puts
    swar2.process!(20, :setup_grid!)
=end
    puts
    q1 = swar3.qap_cost
    q2 = swar3.branch_and_bound!.qap_cost
    puts "branch and bound qap cost: before #{q1}, after #{q2}"
    puts
  end

  def test_branch_and_bound
    skip
    swar = Swar.generate_random(rand(10..100), rand(0.1..0.6))
    q1 = swar.qap_cost
    swar.branch_and_bound!
    q2 = swar.qap_cost
    puts "#{q1}->#{q2}"
    swar.process!
    q3 = swar.qap_cost
    assert(q3 >= q2)
  end

  def test_flow_degree
    skip
    swar = Swar.from_file("pizza_kitchen.swar")
    assert_equal swar.flow_degree(:all_purpose_flour), 4
    assert_equal swar.flow_degree(:pizza_tray), 4
  end

  def test_vertex_properties
    skip
    swar = Swar.from_file("pizza_kitchen.swar")
    swar.show_vertex_table
    swar.process!(60, :setup_grid!)
    swar.show_vertex_table
    puts swar.qap_cost
  end

  def test_ratchet
    # swar = Swar.from_file("pizza_kitchen.swar")
    swar = Swar.generate_random(rand(8..50), rand(0.1..0.6))
    swar.ratchet(36000)
    assert true
  end

end
