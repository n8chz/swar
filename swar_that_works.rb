# require "matrix"
# require "nokogiri"

class Swar
  class VertexNotFound < Exception
  end
  
  class NoOperator < Exception
  end
  
  class MultipleOperators < Exception
  end
  
  class MisplacedOperator < Exception
  end
  
  OPERATORS = {"sa" => 2, "wa" => 1, "nullo" => 0, "wr" => -1, "sr" => -2}

  attr_accessor :graph

  def initialize(vertices = [])
    @graph = {}
    @edge_count = 0
    @total_flow = 0
    # @x, @y, @w, @h = [-1, -1, 2, 2]
    vertices.each do |vertex|
      add_vertex! vertex
    end
    setup_grid!
  end

  # NOTE: methods on vertices take vertex tags as parameters:
  # see https://en.wikipedia.org/wiki/Graph_(abstract_data_type)#Operations

  def adjacent?(x, y)
    f, s = [x.to_sym, y.to_sym].sort
    raise VertexNotFound unless vertices.include? f
    raise VertexNotFound unless vertices.include? s
    !!@graph[f][:edges][s]
  end

  def neighbors(x)
    xs = x.to_sym
    raise VertexNotFound unless has_vertex? xs
    before = @graph.select {|k, v| v[:edges].keys.include?(xs)}.keys
    after = @graph[xs][:edges].keys
    before+after
  end

  def add_vertex!(x)
    xs = x.to_s.to_sym
    @graph[xs] = new_vertex
    self
  end

  def remove_vertex!(x)
    xs = x.to_sym
    @graph.delete xs
    @graph.each do |name, _|
      @graph[name][:edges].delete xs
    end
    self
  end

  def add_edge!(x, y, flow = 1)
    # puts("empty string passed") if x.to_s == ""
    # puts("empty string passed") if y.to_s == ""
    add_vertex!(x) unless has_vertex?(x)
    add_vertex!(y) unless has_vertex?(y)
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    @graph[f][:edges][s] = flow
    @total_flow += flow
    @edge_count += 1
    self
  end

  def remove_edge!(x, y)
    raise VertexNotFound unless has_vertex?(x) && has_vertex?(y)
    f, s = [x.to_sym, y.to_sym].sort
    @graph[f][:edges].delete s
    self
  end

  def get_vertex_value(x)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value]
  end

  def set_vertex_value!(x, val)
    raise VertexNotFound unless has_vertex?(x)
    @graph[x][:value] = val
    self
  end

  # end of methods in Wikipedia's list

  # additional "topological" methods:

  def vertices
    @graph.keys
  end

  # see https://en.wikipedia.org/wiki/Centrality#Harmonic_centrality
  def harmonic_centralities
    @graph.each_with_object({}) do |kv, edges|
      v1, props = kv
      props[:edges].each do |v2, flow|
        edges[v1] ||= 0
        edges[v2] ||= 0
        edges [v1] += flow
        edges [v2] += flow
      end
    end
  end

  # A "stretcher" is defined as a triad of vertices in which
  # two of the three repel each other, while the third is attracted
  # to the first two.
  # Return a list of all such triads in the Swar
  def stretchers
    vertices.combination(3).map do |c|
      w =  c.combination(2).map {|x, y| flow(x, y) <=> 0}
      c.zip(w).sort_by {|_, s| s}
    end.map(&:transpose).select {|_, s| s == [-1, 1, 1]}.map(&:first)
  end
  


  # flow assigned to edge between two vertices
  def flow(x, y)
    return 0 if x == y
    f, s = [x.to_s.to_sym, y.to_s.to_sym].sort
    raise VertexNotFound unless has_vertex?(f) && has_vertex?(s)
    return 0 unless @graph[f]
    @graph[f][:edges][s] || 0
  end

  # qap_cost is defined as
  # the sum of the distances multiplied by the corresponding flows (flows).
  # Goal(s) include(s) minimizing this metric.
  # see https://en.wikipedia.org/wiki/Quadratic_assignment_problem
  def qap_cost
    @graph.map do |v1, p1|
      p1[:edges].map do |v2, p2|
        @layout[v1].dist_to(@layout[v2])*flow(v1, v2)
      end.sum
    end.sum
  end

  def to_s
    @graph.to_s
  end

  # read single line of SWAR file into self
  def process_swar_line!(line)
    tokens = line.sub(/#.*/,"").strip.downcase.split(/\s/)
    return self if tokens.empty? # whitespace lines ok but inconsequential
    parts = tokens.chunk do |x|
      OPERATORS.keys.include? x
    end.to_a.map(&:pop)
    raise NoOperator if parts.count == 1
    raise MisplacedOperator if parts.count == 2
    raise MultipleOperators if parts.count > 3
    left, op, right = parts
    left.product(right).each do |x, y|
      # puts "about to add #{x}, #{y}"
      add_edge!(x.to_sym, y.to_sym, OPERATORS[op[0]])
    end
    self
  end

  # a couple instantiators:

  def self.from_file(filename)
    stream = File.new(filename, "r")
    swar = Swar.new
    while (line = stream.gets)
      swar.process_swar_line!(line)
    end
    swar.setup_grid!
  end

  def self.generate_random(vertex_count, density)
    raise ArgumentError unless vertex_count.integer? && vertex_count.positive?
    raise ArgumentError unless (0.0..1.0).include? density
    edge_count = (density*vertex_count*(vertex_count-1)/2.0).round
    tags = (1..vertex_count).map(&:to_s).map(&:to_sym)
    swar = Swar.new(tags)
    tags.combination(2).to_a.sample(edge_count).each do |x, y|
      # puts "about to add #{x}, #{y}"
      swar.add_edge!(x, y, [-2, -1, 1, 1].sample)
    end
    swar.setup_grid!
  end

  def generate_svg(file_name)
    size = Math.sqrt(@h*@w)
    text_size = size/20
    stroke_width = size/200
    @x -= size*0.1
    @y -= size*0.05
    @w *= 1.2
    @h *= 1.1
    file_name += "%+010.4f" % @self_flow
    file_name += ".svg" unless file_name.end_with? "svg"
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.svg("viewBox" => "#{@x} #{@y} #{@w} #{@h}", "xmlns" => "http://www.w3.org/2000/svg") {
        @graph.each do |k1, v1|
          x1, y1 = v1[:location].to_a
          xml.text_("x" => x1, "y" => y1, "style" => "font-size: #{text_size}px;", "text-anchor" => "middle") {xml.text k1}
          v1[:edges].each do |k2, v2|
            next if flow(k1, k2).zero?
            x2, y2 = @graph[k2][:location].to_a
            xml.line("stroke" => "black", "stroke-width" => "#{stroke_width}", "x1" => x1, "y1" => y1, "x2" => x2, "y2" => y2)
          end
        end
      }
    end
    File.open(file_name, "w") do |file|
      file.write(builder.to_xml)
    end
  end

  def has_vertex?(x)
    vertices.include?(x.to_s.to_sym)
  end

  def process!(timeout = 60, m = :swap2!)
    q = qap_cost
    start = Time.new.to_f
    loop do
      improve!(timeout, m)
      q2 = qap_cost
      t = Time.new.to_f-start
      break if t > timeout
      puts "\"#{m}\",#{t},#{q},#{q2}"
      q = q2
    end
    self
  end

  def improve!(timeout = 60, m = :swap2!)
    q = qap_cost
    start = Time.new.to_f
    loop do
      send(m)
      break if qap_cost < q
      break if Time.new.to_f-start > timeout
    end
    self
  end

  # branch-and-bound utilities:

  def branch_and_bound!(v = vertices)
    @layout = v.inject({}) {|branch, vertex| bound(branch, vertex)}
    self
  end

  def bound(branch, next_vertex)
    # puts "bound(#{branch}, #{next_vertex})"
    open_cells = @grid-branch.values
    branch[next_vertex] = open_cells.min_by do |loc|
      vertices.map do |v|
        flow = flow(v, next_vertex)<=>0
        if flow.zero?
          dist = 0
        elsif branch.keys.include? v
          dist = branch[v].dist_to(loc)
        else
          dists = open_cells.map {|corner| loc.dist_to(corner)}
          dist = flow.negative? ? dists.min : dists.max
        end
        flow*dist
      end.sum
    end
    branch
  end

  # assignment generators that can be passed to #process:

  def setup_grid!
    n = vertices.count
    @n = Math.sqrt(n+1).ceil
    @grid = (0...@n).to_a.product((0...@n).to_a)
    @layout = vertices.zip(@grid.shuffle).to_h
    self
  end

  def swap2!
    a, b = @grid.sample(2)
    x = @layout.key(a)
    y = @layout.key(b)
    @layout[x] = b if x
    @layout[y] = a if y
  end

  private

  def new_vertex
    {edges: {}}
  end

  def place_vertex!(vertex, square)
    @layout.index(square)
  end
end

class Array
  def dist_to(other)
    ds = zip(other).map do |x, y|
      (x-y)*(x-y)
    end.sum
    Math.sqrt(ds)
  end
end

